# Check

Super small and simple check functions for Go testing. Stolen from [Ben Johnson](https://github.com/benbjohnson/testing)

## Install

```
go get gitlab.com/MaxIV/lib-maxiv-go-check
```

## Usage

```golang
package foo

import (
	"testing"

	check "gitlab.com/MaxIV/lib-maxiv-go-check"
)

func TestBar(t *testing.T) {
	expectedValue := 100
	actualValue, err := DoSomething()
	check.OK(t, err)
	check.Equals(t, expectedValue, actualValue)
	check.Assert(t, expectedValue == actualValue, "expected equal")
}

```

## Godoc

```golang
// package check // import "gitlab.com/MaxIV/lib-maxiv-go-check"

func Assert(tb TestingHarness, condition bool)
func AssertWithMessage(tb TestingHarness, condition bool, msg string, v ...interface{})
func Equals(tb TestingHarness, exp, act interface{})
func EqualsWithMessage(tb TestingHarness, exp, act interface{}, msg string, v ...interface{})
func NotOK(tb TestingHarness, err error)
func NotOKWithMessage(tb TestingHarness, err error, msg string, v ...interface{})
func OK(tb TestingHarness, err error)
func OKWithMessage(tb TestingHarness, err error, msg string, v ...interface{})
type TestingHarness interface{ ... }
```
